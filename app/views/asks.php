<!DOCTYPE html>
<html>
<head>
    <title>Hacking news</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('') }}">Inici</a>
        <li><a href="{{ URL::to('submissions/create') }}">Notícies</a>
            <li><a href="{{ URL::to('submissions/create') }}">Preguntes</a>
        <li><a href="{{ URL::to('submissions/create') }}">Crear un nou tema</a>
        <li><a href="{{ URL::to('usuarios/create') }}">Crear un nou Usuari</a>
    </ul>
</nav>

<h1>Temes Actuals</h1>

@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }} </div>
@endif

<table class="table table-striped table-bordered" style="border:0px solid">
    <thead>
        <tr>
            <!--<td>Titulo</td>
            <td>Texto</td>
            <td>URL</td>
            <td>Puntuación</td>
            <td>Autor</td>
            <td>Tipo</td>
            <td>Fecha</td>
        <td>Accion</td>-->
        </tr>
    </thead>
    <tbody>
    <?php $i = 1;
          $like_id ="";   ?>


                
    <script type="text/javascript">
       function submitLike(fila)
        {
            document.getElementById(fila).style.display = "none";
            document.getElementById("fl"+fila).submit();
        }
    </script>

    @foreach($submissions->reverse() as $key => $value)
    
    @if($value->title != null)
        <tr style="border:0px solid">

            <td style="max-width:15px;font-size:20px;vertical-align:middle;border:solid 0px;">{{$i}} <a id="{{$i}}" href="javascript: submitLike({{$i}})"> <img style="margin-bottom:6px;" src="http://icons.iconarchive.com/icons/iconsmind/outline/128/Like-icon.png" alt="Vote" height="20" width="20"> </a></td>
            <td style="vertical-align:middle;border:solid 0px;>
        @if($value->type == 'Pregunta')
            <a href="{{ URL::to('submissions/'.$value->id.'/comments') }}"  style="color: #000000">{{ $value->title }}</a>
        @else
            <a href="{{ URL::to($value->url)}}"  style="color: #000000">{{ $value->title }}</a>
        @endif

            </br>
            <a href="{{ URL::to('submissions/'.$value->id.'/comments') }}" style="font-size:10px">{{$value->score}} points by {{$value->by}} at {{$value->time}} | {{ $value->descendants }} comments</a>
            </td>
            

                {{ Form::model($value, array('action' => array('SubmissionsController@vote', $value->id), 'method' => 'POST', 'id' => 'fl'.$i)) }}

                {{ Form::submit('submit', array('class' => 'btn btn-primary', 'style' => 'display:none')) }}

                {{ Form::close() }}

            <!--<td>{{ $value->title }}</td>
            <td>{{ $value->texto }}</td>
            <td>{{ $value->url }}</td>
            <td>{{ $value->score }}</td>
            <td>{{ $value->by }}</td>
            <td>{{ $value->type }}</td>
            <td>{{ $value->time }}</td>

            <!-- we will also add show, edit, and delete buttons -->
            <!--<td>

                <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                <!-- we will add this later since its a little more complicated than the other two buttons -->
        <!--{{ Form::open(array('url' => 'submissions/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Eliminar', array('class' => 'btn btn-warning')) }}
                {{ Form::close() }}
                <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                <!--<a class="btn btn-small btn-success" href="{{ URL::to('usuarios/' . $value->id) }}">Show this Nerd</a> -->

                <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                <!--<a class="btn btn-small btn-info" href="{{ URL::to('submissions/' . $value->id . '/edit') }}">Editar</a>-->

            <!--</td>-->
        </tr>
        <?php $i = $i +1 ?>
    @endif
    
    @endforeach
    </tbody>
</table>

</div>
</body>
</html>