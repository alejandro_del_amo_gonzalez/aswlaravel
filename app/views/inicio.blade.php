<!DOCTYPE html>
<html>
<head>
    <title>Hacking news</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<?php
 $tipus = "all";
 if(isset($_GET["show"])) {
   $tipus = $_GET["show"];
 }
 $redirection = "show=".$tipus;
 ?>
<div class="container">

<nav class="navbar navbar-inverse">
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('') }}">Inici</a>
        <li><a href="{{ URL::to('?show=news') }}">Notícies</a>
        <li><a href="{{ URL::to('?show=asks') }}">Preguntes</a>
        @if ( ! Auth::user())
            <li><a href="{{ URL::to('facebook') }}">Login amb facebook</a>
        @else
            <li><a href="{{ URL::to('submissions/create') }}">Crear un nou tema</a>
            <li><a href="{{ URL::to('edit_user/'.Auth::user()->username) }}">Editar Perfil</a>
            <li><a href="{{ URL::to('logout') }}">Logout</a>
        @endif
    </ul>
</nav>
@if ($tipus == 'news')
<h1>Notícies</h1>
@elseif($tipus == 'asks')
<h1>Preguntes</h1>
@else
<h1>Temes Actuals</h1>
@endif

@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }} </div>
@endif

<table class="table table-striped table-bordered" style="border:0px solid">
    <thead>
    </thead>
    <tbody>
    <?php $i = 1;
          $like_id ="";   ?>


                
    <script type="text/javascript">
       function submitLike(fila)
        {
            document.getElementById(fila).style.display = "none";
            document.getElementById("fl"+fila).submit();
        }
    </script>

    @foreach($submissions->reverse() as $key => $value)
    
         
    @if($value->title != null && (($value->type == 'Pregunta' && $tipus !='news') || ($value->type == 'URL'  && $tipus != 'asks')))
        <tr style="border:0px solid">
        
        @if(Auth::user() && !in_array(Auth::user()->username,explode(";",$value->likes)))
            <td style="max-width:15px;font-size:20px;vertical-align:middle;border:solid 0px;">{{1}} <a href="{{ URL::to('submissions/'.$value->id.'/likes/'.$redirection.'/direccion') }}"> <img style="margin-bottom:6px;" src="http://icons.iconarchive.com/icons/iconsmind/outline/128/Like-icon.png" alt="Vote" height="20" width="20"> </a></td>
        @else <td style="max-width:15px;font-size:20px;vertical-align:middle;border:solid 0px;"><a href="{{ URL::to('submissions/'.$value->id.'/likes/'.$redirection.'/direccion') }}"> <img style="style=display:none;margin-bottom:6px;" src="http://icons.iconarchive.com/icons/iconsmind/outline/128/Like-icon.png" alt="Vote" height="20" width="20" hidden="true"> </a></td>
        
        @endif
        <td style="vertical-align:middle;border:solid 0px;">
        @if($value->type == 'Pregunta')
            <a href="{{ URL::to('submissions/'.$value->id.'/comments') }}"  style="color: #000000">{{ $value->title }}</a>
        @else
            <a href="{{ URL::to($value->url)}}"  style="color: #000000">{{ $value->title }}</a>
        @endif

            </br>
            <a href="{{ URL::to('submissions/'.$value->id.'/comments') }}" style="font-size:10px">{{count(explode(";",$value->likes))-1}} points by {{$value->by}} at {{$value->time}} | {{ $value->descendants }} comments</a>
            </td>
            

                {{ Form::model($value, array('action' => array('SubmissionsController@vote', $value->id, $redirection), 'method' => 'POST', 'id' => 'fl'.$i)) }}
                {{ Form::close() }}

        </tr>
        <?php $i = $i +1 ?>
    @endif
    
    @endforeach
    </tbody>
</table>

</div>
</body>
</html>