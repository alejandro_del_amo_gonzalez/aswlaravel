<!DOCTYPE html>

<style type="text/css">
<!--
.tab { margin-left: 40px; }
-->
</style>

<html>
<head>
    <title>Hacking news</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('') }}">Inici</a>
        @if ( ! Auth::user())
            <li><a href="{{ URL::to('facebook') }}">Login amb facebook</a>
        @else
            <li><a href="{{ URL::to('submissions/create') }}">Crear un nou tema</a>
            <li><a href="{{ URL::to('usuarios/create') }}">Crear un nou Usuari</a>
            <li><a href="{{ URL::to('edit_user/'.Auth::user()->username) }}">Editar Perfil</a>
            <li><a href="{{ URL::to('logout') }}">Logout</a>
        @endif
    </ul>
</nav>

<h1>Crea una nueva Submission</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

{{ Form::open(array('url' => 'submissions')) }}

    <div class="form-group">
        {{ Form::label('by', 'by') }}
        {{ Form::label('by', Auth::user()->username, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('titulo', 'Titulo') }}
        {{ Form::text('title', Input::old('title'), array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('url', 'URL') }}
        {{ Form::url('url', Input::old('url'), array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('texto', 'Texto') }}
        {{ Form::text('texto', Input::old('texto'), array('class' => 'form-control')) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('tipo', 'Tipo') }}<br>
        <p class="tab"> {{ Form::label('url', 'URL') }}
        {{ Form::radio('tipo', 'URL', array('class' => 'form-control'))}}<br></p>
        <p class="tab">{{ Form::label('pregunta', 'Pregunta') }}
        {{ Form::radio('tipo', 'Pregunta', array('class' => 'form-control'))}}</p>
    </div>


    {{ Form::submit('Crea una Submission!', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

</div>
</body>
</html>

