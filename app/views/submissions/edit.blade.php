<!DOCTYPE html>
<html>
<head>
	<title>Hacking news</title>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
	<ul class="nav navbar-nav">
        <li><a href="{{ URL::to('') }}">Inicio</a>
        <li><a href="{{ URL::to('submissions/create') }}">Crear una nueva Submission</a>
		<li><a href="{{ URL::to('usuarios/create') }}">Crear un nuevo Usuario</a>
	</ul>
</nav>

<h1>Editar {{ $submission->title }}</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

{{ Form::model($submission, array('action' => array('SubmissionsController@update', $submission->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('username', 'username') }}
        {{ Form::text('by', Input::old('by'), array('class' => 'form-control')) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('titulo', 'Titulo') }}
        {{ Form::text('title', Input::old('title'), array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('url', 'URL') }}
        {{ Form::url('url', Input::old('url'), array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('texto', 'Texto') }}
        {{ Form::text('texto', Input::old('texto'), array('class' => 'form-control')) }}
    </div>

	{{ Form::submit('Guardar Cambios', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

</div>
</body>
</html>
