<!DOCTYPE html>
<html>
<head>
    <title>Hacking news</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>


    <script type="text/javascript">
       function submitLike(fila)
        {
            document.getElementById(fila).style.display = "none";
            document.getElementById("fl"+fila).submit();
        }
    </script>
    
<div class="container">
<nav class="navbar navbar-inverse">
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('') }}">Inici</a>
        @if ( ! Auth::user())
            <li><a href="{{ URL::to('facebook') }}">Login amb facebook</a>
        @else
            <li><a href="{{ URL::to('submissions/create') }}">Crear un nou tema</a>
            <li><a href="{{ URL::to('edit_user/'.Auth::user()->username) }}">Editar Perfil</a>
            <li><a href="{{ URL::to('logout') }}">Logout</a>
        @endif
    </ul>
</nav>

<?php

    $hash_array = array();
    $count = 0;
    foreach ($submissions as $value) {
        $hash_array[$value->id] = $value;
    }
    $redirection = $submission->id;
    $comentaris = "";
    foreach (array_reverse($hash_array) as $value) {

            ?>
            {{ Form::model($value, array('action' => array('SubmissionsController@vote', $value->id, $redirection), 'method' => 'POST', 'id' => 'fl'.$value->id)) }}
            {{ Form::close() }}
            <?php

        if ($value->parent == $submission->id) {

           $comentaris.=generarArbre($hash_array, $value->id, "100", $count,$redirection);
        }   
    }

    function generarArbre(&$hash_array, $id, $width, &$count,$redirection) {
        $contingut = "";
        $count = $count +1;
        $value = $hash_array[$id];
        $contingut.='<div style="border:0px solid;margin-left:'.(100-$width).'%;width:'.$width.'%; min-width:300px;margin-top:4px;">';
       // $redirection = 'submissions/'.$submission->id.'/comments';
        
        if($value->descendants == 0) {
            $contingut.='<div style="border:0px solid;padding:5px">';
            $contingut.='<a style="color:grey;font-size:12px;" href="/../../usuarios/'.$value->by.'/">'.$value->by.'</a><a href="/../../submissions/'.$value->id.'/comments" style="color:grey;font-size:12px;"> at '.$value->time.'</a>';
            
            $contingut.='<td style="color:grey;font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;'.(count(explode(";",$value->likes))-1).'</td>';
            
            if(!in_array(Auth::user()->username,explode(";",$value->likes)))$contingut.='<a id="'.$value->id.'" href="/../../usuarios/'.$value->by.'/upgradeKarma/'.$value->id.'/likes/'.$redirection.'/direccion"><img style="margin-bottom:6px;" src="http://icons.iconarchive.com/icons/iconsmind/outline/128/Like-icon.png" alt="Vote" height="12" width="12"> </a>';
            else $contingut.='<img style="margin-bottom:6px;" src="http://iconshow.me/media/images/Application/Modern-Flat-style-Icons/png/512/Like.png" alt="Vote" height="12" width="12">';
            
            $contingut.='<p style="color:black;">'.$value->texto.'</p>';
            $contingut.='<a href="/../../submissions/'.$value->id.'/comments" style="color:black;font-size:12px;text-decoration: underline;">Contestar</a>';
            $contingut.='</div>';
        }
        else {
            $contingut.='<div style="border:0px solid;padding:5px">';
            $contingut.='<a style="color:grey;font-size:12px;" href="/../../usuarios/'.$value->by.'/">'.$value->by.'</a><a href="/../../submissions/'.$value->id.'/comments" style="color:grey;font-size:12px;"> at '.$value->time.'</a>';
             
            $contingut.='<td style="color:grey;font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;'.(count(explode(";",$value->likes))-1).'</td>';
            
            
            if(!in_array(Auth::user()->username,explode(";",$value->likes)))$contingut.='<a id="'.$value->id.'" href="/../../submissions/'.$value->id.'/likes/'.$redirection.'/direccion"><img style="margin-bottom:6px;" src="http://icons.iconarchive.com/icons/iconsmind/outline/128/Like-icon.png" alt="Vote" height="12" width="12"> </a>';
            else $contingut.='<img style="margin-bottom:6px;" src="http://iconshow.me/media/images/Application/Modern-Flat-style-Icons/png/512/Like.png" alt="Vote" height="12" width="12">';

            
            $contingut.='<p style="color:black";>'.$value->texto.'</p>';
            $contingut.='<a href="/../../submissions/'.$value->id.'/comments" style="color:black;font-size:12px;text-decoration: underline;">Contestar</a>';
            $contingut.='</div>';
            $fills = explode(";",$value->kids);
            foreach(array_reverse($fills) as $fill_id) {
                if($fill_id != 0) {
                    $contingut.= generarArbre($hash_array, $fill_id, "96", $count, $redirection);
                }
            }
        }
        $contingut.="</div>";
        return $contingut;

    }

?>


<div class="container" style="background-color:rgb(240,240,240);">

<h1>Comentaris</h1>

<div>
    <div>
        @if ($submission->title != "")
            <h4>{{ $submission->title }} ({{ $submission->url }})</h4>
        @endif
        <p>{{ count(explode(";",$value->likes))-1 }} points by <a href="{{ URL::to('usuarios/'.$value->by) }}">{{ $submission->by}}</a> at {{ $submission->time}} | {{ $count}} comments</p>
        @if ($submission->title == "")
            <p>{{ $submission->texto }}</p>
        @endif
    </div>
    <div>
        {{ HTML::ul($errors->all()) }}

            {{ Form::model($submission, array('action' => array('SubmissionsController@store_comment', $submission->id), 'method' => 'POST')) }}
                <div class="form-group">
                    {{ Form::text('texto',"", array('class' => 'form-control')) }}
                </div>

                {{ Form::submit('Add comment', array('class' => 'btn btn-primary')) }}

            {{ Form::close() }}
    </div>
</div>
</br>
</br>
<div style="margin-bottom:50px">

{{$comentaris}} 


</div>
</div>
</body>
</html>
