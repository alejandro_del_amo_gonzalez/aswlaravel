<!DOCTYPE html>
<html>
<head>
    <title>Hacking news</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('') }}">Inici</a>
        @if ( ! Auth::user())
            <li><a href="{{ URL::to('facebook') }}">Login amb facebook</a>
        @else
            <li><a href="{{ URL::to('submissions/create') }}">Crear un nou tema</a>
            <li><a href="{{ URL::to('edit_user/'.Auth::user()->username) }}">Editar Perfil</a>
            <li><a href="{{ URL::to('logout') }}">Logout</a>
        @endif
    </ul>
</nav>

<h1>Usuari: {{$usuario->username}}</h1>

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif


      <h5>Nom: {{ $usuario->username }}</h5>
      <h5>Data de creació: {{ $usuario->created_date }}</h5>
      <h5>Karma: {{ $usuario->karma }}</h5>
      <h5>Email: {{ $usuario->email }}</h5>
      <h5>Informació: {{ $usuario->about }}</h5>

</div>
</body>
</html>
