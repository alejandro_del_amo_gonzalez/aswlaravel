<!DOCTYPE html>
<html>
<head>
    <title>Hacking news</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('') }}">Inicio</a>
        <li><a href="{{ URL::to('submissions/create') }}">Crear una nueva Submission</a>
        <li><a href="{{ URL::to('usuarios/create') }}">Crear un nuevo Usuario</a>
        @if ( ! Auth::getUser())
            <li><a href="{{ URL::to('facebook') }}">Login amb facebook</a>
        @endif
    </ul>
</nav>

<h1>Todos los usuarios</h1>

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>Nombre</td>
            <td>Email</td>
	    <td>Accion</td>
        </tr>
    </thead>
    <tbody>
    @foreach($usuarios as $key => $value)
        <tr>
            <td>{{ $value->username }}</td>
            <td>{{ $value->email }}</td>

            <!-- we will also add show, edit, and delete buttons -->
            <td>

                <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                <!-- we will add this later since its a little more complicated than the other two buttons -->
		{{ Form::open(array('url' => 'usuarios/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Eliminar', array('class' => 'btn btn-warning')) }}
                {{ Form::close() }}
                <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                <!--<a class="btn btn-small btn-success" href="{{ URL::to('usuarios/' . $value->id) }}">Show this Nerd</a> -->

                <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                <a class="btn btn-small btn-info" href="{{ URL::to('usuarios/' . $value->id . '/edit') }}">Editar</a>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>

</div>
</body>
</html>
