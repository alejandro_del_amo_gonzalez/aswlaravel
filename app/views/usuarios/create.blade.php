<!DOCTYPE html>
<html>
<head>
    <title>Hacking news</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('') }}">Inicio</a>
        <li><a href="{{ URL::to('submissions/create') }}">Crear una nueva Submission</a>
        <li><a href="{{ URL::to('usuarios/create') }}">Crear un nuevo Usuario</a>
    </ul>
</nav>

<h1>Crea a Usuario</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

{{ Form::open(array('url' => 'usuarios')) }}

    <div class="form-group">
        {{ Form::label('username', 'username') }}
        {{ Form::text('username', Input::old('username'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('email', 'Email') }}
        {{ Form::email('email', Input::old('email'), array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('password', 'password') }}
        {{ Form::text('password', Input::old('password'), array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Crea un Usuario!', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

</div>
</body>
</html>

