
<!DOCTYPE html>
<html>
<head>
	<title>Hacking news</title>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('') }}">Inici</a>
        @if ( ! Auth::user())
            <li><a href="{{ URL::to('facebook') }}">Login amb facebook</a>
        @else
            <li><a href="{{ URL::to('submissions/create') }}">Crear un nou tema</a>
            <li><a href="{{ URL::to('edit_user/'.Auth::user()->username) }}">Editar Perfil</a>
            <li><a href="{{ URL::to('logout') }}">Logout</a>
        @endif
    </ul>
</nav>

<h1>Editar {{ $usuario->username }}</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

{{ Form::model($usuario, array('action' => array('UsuariosController@update', $usuario->id), 'method' => 'PUT')) }}

	<div class="form-group">
		{{ Form::label('username', 'Nom') }}
		{{ Form::label('name', $usuario->username, array('class' => 'form-control')) }}
	</div>

	<div class="form-group">
		{{ Form::label('email', 'Email') }}
		{{ Form::email('email', null, array('class' => 'form-control')) }}
	</div>

	<div class="form-group">
		{{ Form::label('about', 'Informació') }}
		{{ Form::textarea('about', null, array('class' => 'form-control')) }}
	</div>

	{{ Form::submit('Guardar Cambios', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

</div>
</body>
</html>