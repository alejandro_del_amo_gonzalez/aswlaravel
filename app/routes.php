<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	//return View::make('hello');
	$submissions = Submission::all();

	return View::make('inicio')->with('submissions', $submissions);
});


/*Route::get('submissions/comments', function()
{
    return View::make('submissions.comments');
});*/

Route::get('submissions/1/reply', function()
{
	//$submission = Submission::find($id); 
    return View::make('submissions.reply');//->with('submission', $submission);
});
/*
Route::get('usuarios', function()
{
	return View::make('usuarios.index');
});*/

Route::resource('usuarios', 'UsuariosController');
Route::resource('api', 'ApiController',array('only'=>array('getSubmissions','getUsers','getSubmission','getUser','doLike','updateUser','createUser','deleteUser','deleteSubmission', 'createNews', 'createAsks', 'reply','updateNews','updateAsks')));
Route::resource('submissions', 'SubmissionsController',array('only'=>array('index','create','store','edit','update','vote')));

Route::get('submissions/{id}/comments', 'SubmissionsController@comments');
Route::get('usuarios/{username}/', 'UsuariosController@show');
Route::post('submissions/{id}/comments', 'SubmissionsController@store_comment');
Route::post('submissions/{id}/votes/{redirection}', 'SubmissionsController@vote');
Route::get('submissions/{id}/likes/{redirection}/direccion', 'SubmissionsController@likes');
Route::get('edit_user/{username}/', 'UsuariosController@edit');
Route::get('facebook', 'UsuariosController@loginFacebook');
Route::get('usuarios/{username}/upgradeKarma/{id}/likes/{redirection}/direccion','UsuariosController@upgradeKarma');



Route::get('api/submissions', 'ApiController@getSubmissions');
Route::get('api/submissions/{id}/', 'ApiController@getSubmission');
Route::get('api/usuarios', 'ApiController@getUsers');
Route::get('api/usuarios/{username}/', 'ApiController@getUser');
Route::post('api/usuarios/crear', 'ApiController@createUser');
Route::post('api/usuarios/modificar', 'ApiController@updateUser');
Route::post('api/usuarios/eliminar', 'ApiController@deleteUser');
Route::post('api/submissions/eliminar', 'ApiController@deleteSubmission');
Route::post('api/submissions/create/news', 'ApiController@createNews');
Route::post('api/submissions/create/asks', 'ApiController@createAsks');
Route::post('api/submissions/reply/', 'ApiController@reply');
Route::post('api/submissions/modificar/news', 'ApiController@updateNews');
Route::post('api/submissions/modificar/ask', 'ApiController@updateAsks');
Route::post('api/submissions/like', 'ApiController@doLike');



Route::get('logout', function() {
    Auth::logout();
    return Redirect::to('/');
});