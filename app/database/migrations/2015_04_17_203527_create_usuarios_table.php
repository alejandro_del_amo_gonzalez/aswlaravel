<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsuariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usuarios', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('faceId');
			$table->date('created_date');
			$table->string('username');
			$table->string('password');
			$table->string('email');
			$table->integer('karma');
			$table->string('about');
           // required for Laravel 4.1.26
          	$table->string('remember_token', 100)->nullable();
			/*submitted*/
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('usuarios');
	}

}
