<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubmissionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('submissions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->boolean('deleted');	
			$table->string('type');
			$table->string('by');
			$table->date('time');
			$table->text('texto');
			$table->integer('parent');
			$table->string('url');
			$table->integer('score');
			$table->string('title');
			$table->string('likes');
			$table->string('kids'); //id separats per ;
			$table->integer('descendants');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('submissions');
	}

}
