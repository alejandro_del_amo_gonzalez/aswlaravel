<?php

use Carbon\Carbon;

class SubmissionsController extends \BaseController {

	/**
	 * Display a listing of submissions
	 *
	 * @return Response
	 */
	public function index()
	{
		$submissions = Submission::all();

		return View::make('submissions.index', compact('submissions'));
	}

	/**
	 * Show the form for creating a new submission
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('submissions.create');
	}

	/**
	 * Store a newly created submission in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if (Input::get('tipo')=='URL'){
			$rules = array(
		    	'title'      => 'required',
		    	'url' => 'required|url|unique:submissions,url'
			);
		}
		else { //quan és pregunta
			$rules = array(
		    	'title'      => 'required',
		    	'url' => 'max:0'
			);
		}
		

		$validator = Validator::make($data = Input::all(), $rules);

		if ($validator->fails())
		{
			return Redirect::to('submissions/create')->withErrors($validator)
			->withInput();
		}
		else
		{
			// store
		    $submissions = new Submission;
		    $submissions->deleted = false;
		    $submissions->type = Input::get('tipo');
			$submissions->by = Auth::user()->username;
			$submissions->time = Carbon::now();
			$submissions->texto = Input::get('texto');
			$submissions->parent = 0;
			$submissions->url = Input::get('url');
			$submissions->score = 0;
			$submissions->kids = "";
			$submissions->likes ="";
			$submissions->title = Input::get('title');
			$submissions->descendants = 0;
		    $submissions->save();

		    // redirect
		    Session::flash('message', 'You successfully created a submission!');
		    return Redirect::to('submissions/'.$submissions->id.'/comments');
		}
	}

	/**
	 * Display the specified submission.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$submission = Submission::findOrFail($id);

		return View::make('submissions.show', compact('submission'));
	}

	/**
	 * Show the form for editing the specified submission.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$submission = Submission::find($id);

		return View::make('submissions.edit', compact('submission'));
	}

  /**
	 * Show the form for reply the specified submission.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function reply($id)
	{
		$submission = Submission::find($id);

		return View::make('submissions.reply', compact('submission'));
	}

  /**
	 * Show the form for comment the specified submission.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function comments($id)
	{
		//$result = array();
		$submission = Submission::find($id);
		$submissions = Submission::all();
		//$comments = Submission::where('parent','=',$id)->get();
	
		//$posicion = 0;
		//$resultado = new array();
		//$resultado->arbol($result,$posicion,$id);

		


		return View::make('submissions.comments',compact('submission','submissions'));

	}



	/**
	 * Update the specified submission in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		$rules = array(
		    'by'       => 'required|exists:usuarios,username',
		    'title'      => 'required',
		    'url' => 'required|url|unique:submissions,url,'.$id
		);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()) {
		    return Redirect::to('submissions/' . $id . '/edit')
		        ->withErrors($validator)
		        ->withInput();
		} else {
		    // store

		    $submissions = Submission::find($id);
			$submissions->by = Input::get('by');
			$submissions->texto = Input::get('texto');
			$submissions->url = Input::get('url');
			$submissions->title = Input::get('title');
		    $submissions->save();

		    // redirect
		    Session::flash('message', 'Cambios guardados!');
		    return Redirect::to('submissions');
		}

		$submission = Submission::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Submission::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$submission->update($data);

		return Redirect::route('submissions.index');
	}

	/**
	 * Remove the specified submission from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Submission::destroy($id);

		return Redirect::route('submissions.index');
	}



	/**
	 * Store a newly created submission in storage.
	 *
	 * @return Response
	 */
	public function store_comment($id)
	{

		$rules = array(
		    'texto'      => 'required'
		);

		$validator = Validator::make($data = Input::all(), $rules);

		if ($validator->fails())
		{
			return Redirect::to('submissions.comments')->withErrors($validator)
			->withInput();
		}
		else
		{
			// store
		    $submissions = new Submission;
		    $submissions->deleted = false;
		    $submissions->type = 'ninguno';
			$submissions->by = Auth::user()->username;
			$submissions->time = Carbon::now();
			$submissions->texto = Input::get('texto');
			$submissions->parent =  $id;
			$submissions->url = '';
			$submissions->score = 0;
			$submissions->kids = '';
			$submissions->title = '';
			$submissions->likes = "";
			$submissions->descendants = 0;
		    $submissions->save();

		    $submissionsp = Submission::find($id);
		    $submissionsp->kids = $submissionsp->kids.$submissions->id.";";
		    $submissionsp->descendants = $submissionsp->descendants + 1;
		    $submissionsp->save();

		    // redirect
		    Session::flash('message', 'You successfully created a comment!');
		    return Redirect::to('submissions/'.$id.'/comments');
		}
	}

	public function vote($id, $redirection){
		$submissions = Submission::find($id);
		$submissions->score += 1;
		$submissions->save();
		// redirect
		return Redirect::to($redirection);

	}
	public function likes($id, $redirection){
		$submissions = Submission::find($id);
		$submissions->likes =$submissions->likes.Auth::user()->username.";";
		$submissions->save();
		// redirect
		
		if(strpos($redirection,'show')!== false) return Redirect::to("?".$redirection);
		else return Redirect::to('submissions/'.$redirection.'/comments');

	}
	
/*

	public function arbol($array,$posicion,$id) {
		$array[$hijo->id] = $position;
		$hijos = Submission::where('parent','=',$id)->get();
		foreach($hijos as $aux) {
			$valor = $posicion++;
			crearArbol($array,$valor,$aux->id);
		}
		return $array;
	}*/

	

}
