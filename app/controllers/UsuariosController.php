<?php

use Carbon\Carbon;

class UsuariosController extends \BaseController {

	/**
	 * Display a listing of usuarios
	 *
	 * @return Response
	 */
	 
	//establecemos restful a true
	public $restful = true;
	
	public function index()
	{
		$usuarios = Usuario::all();

		return View::make('usuarios.index')->with('usuarios', $usuarios);
	}

	/**
	 * Show the form for creating a new usuario
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('usuarios.create');
	}

	/**
	 * Store a newly created usuario in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
	
		$rules = array(
		    'username'       => 'required',
		    'email'      => 'required|email',
		    'password' => 'required'
		);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()) {
		    return Redirect::to('usuarios/create')
		        ->withErrors($validator)
		        ->withInput(Input::except('password'));
		} else {
		    // store
		    $usuario = new Usuario;
		    $usuario->username       = Input::get('username');
		    $usuario->password       = Input::get('password');
		    $usuario->created_date =  Carbon::now();
		    $usuario->email      = Input::get('email');
		    $usuario->karma  = 1;
		    $usuario->about  = "nose";
		    $usuario->save();

		    // redirect
		    Session::flash('message', 'Se ha creado correctamente!');
		    return Redirect::to('usuarios');
		}
    
	}

	/**
	 * Display the specified usuario.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($username)
	{
		$usuario = Usuario::where('username', $username)->first();

		return View::make('usuarios.show', compact('usuario'));
	}

	/**
	 * Show the form for editing the specified usuario.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($username)
	{
		if($username==Auth::user()->username) {
			$usuario = Usuario::where('username', $username)->first();
			return View::make('usuarios.edit')->with('usuario', $usuario);	
		}
		else Redirect::to('/');
		
	}

	/**
	 * Update the specified usuario in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
		);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()) {
			$nombre_usu = utf8_encode(Auth::user()->username);
		    return Redirect::to('edit_user/' . $nombre_usu)
		        ->withErrors($validator)
		        ->withInput(Input::except('password'));
		} else {
		    // store
		    $usuario = Usuario::find($id);
		    $usuario->email      = Input::get('email');
		    $usuario->about	= Input::get('about');
		    $usuario->save();

		    // redirect
		    Session::flash('message', 'Cambios guardados!');
		    return Redirect::to('/');
		}
    	}

	/**
	 * Remove the specified usuario from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$usuario = Usuario::find($id);
        	$usuario->delete();

	       	 // redirect
		Session::flash('message', 'Se ha borrado correctamente!');
		return Redirect::to('usuarios');
	}

	public function loginFacebook() {
		$code = Input::get('code');

    	// get fb service //FALTA URL
    	$fb = OAuth::consumer('Facebook');

    	//check for valid code
    	// if empty provide user data
        if ( !empty($code)){
            $token = $fb->requestAccessToken($code);

            //send a request with it
            $me = json_decode( $fb->request('/me'), true);

			$usuario = Usuario::where('username', $me['name'])->first();

		
			if (empty($usuario)) {
       			$user = new Usuario;
       			//$user->$faceId = '12345';//(string)$me['id'];
       			$user->username = $me['name'];
       			$user->password = '1234';
		    	$user->created_date = Carbon::now();
       			//$user->email = $me['email'];
		   		$user->karma = 1;
		    	$user->about = "nose";
       			$user->save();
   				$usuario = $user;
   			}
   			echo $usuario->username;
   			echo $usuario->password;

			/*$attempt = 
					Auth::attempt(array(
				'username' => $usuario->username,
				'password' => $usuario->password,
				));
				
			if ($attempt)
			{
   				return Redirect::to('/')->with('message', 'Logged in with Facebook');
			}else{
				return Redirect::to('/')->with('message', 'Error');
			}*/
  			
  			//
   			Auth::login($usuario, true);
			return Redirect::to('/')->with('message', 'Logged in with ' . Auth::user()->username);
        	//Var_dump
        	//display whole array().
        	//dd($result);
    	}
    	else {
            //get fb authorization
        	$url = $fb->getAuthorizationUri();

        	// return to facebook login url
        	return Redirect::to((string)$url);
    	}
	}
	
	public function upgradeKarma($username,$comment_id,$submission_id) {
		
		$usuario = Usuario::where('username', $username)->first();
		$usuario->karma	= $usuario->karma +1;
		$usuario->save();
		
		return Redirect::to('/submissions/'.$comment_id.'/likes/'.$submission_id.'/direccion');
																								//Estas modificando??
		//return Redirect::routes('SubmissionsController@likes',array('redirect' => $submission_id,'id' => $comment_id));
		
		
		//Route::get('/submissions/'.$submission_id.'/likes/'.$comment_id.'/direccion','SubmissionsController@likes');
		
	}

}
