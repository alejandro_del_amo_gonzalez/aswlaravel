<?php

class ApiController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
		public function createUser()
	{
		
		if(!Usuario::where('username', Request::get('username'))){
		$user = new Usuario;
		$user->username = Request::get('username');
		$user->email = Request::get('email');
		$user->password = Request::get('password');
	    $user->karma  = 1;
	    $user->created_date =  Carbon::now();
	    $user->about  = "Introdueix alguna cosa sobre tu";
		$user->save();
		
		
		return Usuario::find($user->id);
		
		}
		
		else return Response::json(array(
        'error' => true,
        'message' => 'username already in use' ),
        400
    );

	}
	
	
		public function createNews()
	{
			$submissions = new Submission;
	
			$error = false;
			
			if (!Request::get('url') ||  !Request::get('title')) $error = true;
			if (filter_var(Request::get('url'), FILTER_VALIDATE_URL) === false) $error = true;
			if ($error == false) {
			    $submissions = new Submission;
			    $submissions->deleted = false;
				$submissions->by = 'Anònim';
				$submissions->time = Carbon::now();
				$submissions->type = 'URL';
				$submissions->texto = '';
				$submissions->parent = 0;
				$submissions->url = Request::get('url');
				$submissions->score = 0;
				$submissions->kids = "";
				$submissions->likes ="";
				$submissions->title = Request::get('title');
				$submissions->descendants = 0;
			    $submissions->save();
			    
			    return Submission::find($submissions->id);
			}
			else {
				return Response::json(array(
			        'error' => true,
        			'missatge' => "Dades incorrectes"),
    				 400);
			}
		
		
		

	}
	
	
		public function createAsks()
	{
			$submissions = new Submission;
	
			$error = false;
			
			if (!Request::get('text') ||  !Request::get('title')) $error = true;
			if ($error == false) {
			    $submissions = new Submission;
			    $submissions->deleted = false;
				$submissions->by = 'Anònim';
				$submissions->time = Carbon::now();
				$submissions->type = 'Pregunta';
				$submissions->texto = Request::get('text');
				$submissions->parent = 0;
				$submissions->url = '';
				$submissions->score = 0;
				$submissions->kids = "";
				$submissions->likes ="";
				$submissions->title = Request::get('title');
				$submissions->descendants = 0;
			    $submissions->save();
			    
			    return Submission::find($submissions->id);
			}
			else {
				return Response::json(array(
			        'error' => true,
        			'missatge' => "Dades incorrectes"),
    				 400);
			}
	}
		
			public function reply()
	{
			$submissions = new Submission;
	
			
			if (Request::get('text') && Request::get('parent')) {
			    $submissions = new Submission;
			    $submissions->deleted = false;
				$submissions->by = 'Anònim';
				$submissions->time = Carbon::now();
				$submissions->type = 'ninguno';
				$submissions->texto = Request::get('text');
				$submissions->parent = Request::get('parent');
				$submissions->url = "";
				$submissions->score = 0;
				$submissions->kids = "";
				$submissions->likes ="";
				$submissions->title = "";
				$submissions->descendants = 0;
			    $submissions->save();
			    
			    return Submission::find($submissions->id);
			}
			else {
				return Response::json(array(
			        'error' => true,
        			'missatge' => "Dades incorrectes"),
    				 400);
			}
		
		
		

	

	}
	
	

	public function updateUser()
{
	
	$id = Request::get('id');
    $user = Usuario::find($id);
 
    if ( Request::get('about') ){
        $user->about = Request::get('about');
    }
 
    if ( Request::get('password') ){
        $user->password = Request::get('password');
    }
    
    if ( Request::get('email') ){
        $user->email = Request::get('email');
    }
    
    $user->save();
 
    return Response::json(array(
        'error' => false,
        'usuarios' => $user->toArray() ),
        200
    );
}

	public function deleteUser()
	{
	
	if(Usuario::find(Request::get('id'))){
	$id = Request::get('id');

	$user = Usuario::find($id);
 
    $user->delete();
 
    return Response::json(array(
        'error' => false,
        'message' => 'User deleted'),
        200
        );
        
	}
	else return Response::json(array(
        'error' => true,
        'message' => 'User not found'),
        400
        );
	}
	
	
	public function deleteSubmission()
	{
		
		if(Submission::find(Request::get('id'))){
		$id = Request::get('id');
	
		$submission = Submission::find($id);
	 
	    $submission->delete();
	 
	    return Response::json(array(
	        'error' => false,
	        'message' => 'Submission deleted successfully'),
	        200
	        );
	        
		}
		else {
			return Response::json(array(
	        'error' => true,
	        'message' => 'Submission not found'),
	        400
	        );
		}
	}
	
	public function getSubmissions()
	{

	$submissions = Submission::all();
	return Response::json(array(
        'error' => false,
        'submissions' => $submissions->toArray()),
        200
    );

	}
	
	public function getSubmission($id)
	{
		return Submission::find($id);

	}
	
	public function getUsers()
	{
		return Usuario::all();

	}
	
	public function getUser($username)
	{
		return Usuario::where('username', $username)->first();

	}

	public function updateNews()
	{
		
		$id = Request::get('id');
	   
	    if(Submission::find($id)){
	   
		    $submission = Submission::find($id);

		 	
			    if ( Request::get('title') ){
			        $submission->title = Request::get('title');
			    }
			 
			    if ( Request::get('url') ){
			        $submission->url = Request::get('url');
			    }

		 	
		 	
		    
		    $submission->save();
		 
		    return Response::json(array(
		        'error' => false,
		        'submission' => $submission->toArray() ),
		        200
		    );
				    
	    }
	    
	    else  return Response::json(array(
				        'error' => true,
				        'message' => 'news not found' ),
				        400
				    );
	}
	
	
	public function updateAsks()
	{
		
		$id = Request::get('id');
	   
	    if(Submission::find($id)){
	   
		    $submission = Submission::find($id);
		 	
		 		 if ( Request::get('title') ){
			        $submission->title = Request::get('title');
			    }
			 
			    if ( Request::get('text') ){
			        $submission->texto = Request::get('text');
			    }
		 		
		    $submission->save();
		 
		    return Response::json(array(
		        'error' => false,
		        'submission' => $submission->toArray() ),
		        200
		    );
				    
	    }
	    
	    else  return Response::json(array(
				        'error' => true,
				        'message' => 'submission not found' ),
				        400
				    );
	}
	
	
	public function doLike(){
		
		$id = Request::get('id');
	   
	    if(Submission::find($id)){
	    	
	    	$submission = Submission::get($id);
	    	$aux = $submission->likes;
	    	$submission->likes = $submission->likes . "Anonim;";
		
			$submission -> save();
			
			return Response::json(array(
		        'error' => false,
		        'submission' => $submission->toArray() ),
		        200
		    );
		
		}
		
		else return Response::json(array(
		        'error' => true,
		        'submission' => 'submission not found' ),
		        200
		    );
	}

}
